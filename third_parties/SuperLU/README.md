##installazione

- scaricare i sorgenti con il comando `superlu-download` attenzione per il download uso `bash` che viene con il comando `git`
- compilare i sorgenti con il comando `superlu-compile-vs2013`
  per Visual Studio 2013 e `superlu-compile-vs2015` per 
  Visual Studio 2015
- installare le librerie e gli header corrispondenti con i
  comandi `superlu-install-vs2013` e `superlu-install-vs2015`
